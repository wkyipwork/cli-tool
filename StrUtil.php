<?php
include "ExportUtil.php";

class StrUtil extends ExportUtil {

    protected $input_str = '';
    protected $arr = array();

    public function __construct($input_str = '') {
        $this->input_str = $input_str;
        $this->arr = str_split($input_str);
    }

    public function convertStrUpper() {
        return strtoupper($this->input_str);
    }

    public function convertStrAlternate() {
        $str = '';
        foreach( $this->arr as $i => $char ) {
            if( $i %2 == 0 ) {
                $str .= strtolower($char);
            } else {
                $str .= strtoupper($char);
            }
        }

        return $str;
    }

}



