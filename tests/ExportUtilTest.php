<?php
use PHPUnit\Framework\TestCase;
require "ExportUtil.php";

class ExportUtilTest extends TestCase
{
    public function testUppercase()
    {
        $strUtil = new StrUtil('My heart will go on');

        $this->assertEquals('MY HEART WILL GO ON', $strUtil->convertStrUpper());
    }

    public function testAlternate()
    {
        $strUtil = new StrUtil('My heart will go on');

        $this->assertEquals('mY HeArT WiLl gO On', $strUtil->convertStrAlternate());
    }
}
?>