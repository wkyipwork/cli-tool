<?php
use PHPUnit\Framework\TestCase;
require "StrUtil.php";

class StrUtilTest extends TestCase
{
    public function testUppercase()
    {
        $strUtil = new StrUtil('My heart will go on');

        $this->assertEquals('MY HEART WILL GO ON', $strUtil->convertStrUpper());
    }

    public function testAlternate()
    {
        $strUtil = new StrUtil('My heart will go on');

        $this->assertEquals('mY HeArT WiLl gO On', $strUtil->convertStrAlternate());
    }

    public function testExportCsvResponse() {
        $strUtil = new StrUtil('My heart will go on');

        $this->assertEquals('CSV created!', $strUtil->generateCsv());
    }

    public function testExportedFileExist() {
        $expectedFileName = 'export-My heart will go on-csv-' . date('YmdHis') . '.csv';

        $this->assertFileExists( 
            $expectedFileName, 
            "given filename doesn't exists"
        );
    }
}
?>