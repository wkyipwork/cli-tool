# Short Accessment by iPrice

Using object-oriented concepts to create a CLI tool under certain requirements.

## Installation
To install PHP 7 from composer.
```
php composer.phar install
```

## Usage

To get the expected response, open command prompt and type as below:
```
php index.php input="hello world"
```
Sample Output
```
HELLO WORLD
hElLo wOrLd
CSV created!
```

For unit test, kindly type as below:
```
.\vendor\bin\phpunit tests/StrUtilTest.php
```
