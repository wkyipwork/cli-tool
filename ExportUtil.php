<?php
class ExportUtil {

    private $rawStr = '';
    private $filename = '';

    public function generateCsv() {
        $this->rawStr = self::convertStrCsv();
        $this->filename = self::genFileName();

        $csv = self::exportCsv();

        if( $csv ) {
            return 'CSV created!';
        } else {
            return false;
        }
    }

    public function exportCsv() {
        $path = dirname(__FILE__) . '/' . $this->filename;
        $fp = fopen( $path, "w" );
        fwrite( $fp, $this->rawStr ); 
        fclose( $fp );
        
        if( file_exists($path) ) {
            return true;
        } else {
            return false;
        }
    }

    public function genFileName() {
        $filename = 'export-' . $this->input_str . '-csv-' . date('YmdHis') . '.csv';
        return $filename;
    }

    public function convertStrCsv() {
        $str = implode(',', $this->arr);
        return $str;
    }
}