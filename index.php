<?php
include "StrUtil.php";

parse_str(implode('&', array_slice($argv, 1)), $_GET);

$input_str = 'default string here';
if( isset($_GET['input']) && !empty($_GET['input']) ) $input_str = $_GET['input'];

$strUtil = new StrUtil($input_str);

echo $strUtil->convertStrUpper() . PHP_EOL;
echo $strUtil->convertStrAlternate() . PHP_EOL;

$exported = $strUtil->generateCsv();
if( $exported ) echo $exported . PHP_EOL;
